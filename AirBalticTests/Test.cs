﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;


namespace AirBalticTests
{
    [TestFixture]

    public class Test
    {
        protected IWebDriver driver;
        protected string baseUrl = "https://airbaltic.com";
        private IWait<IWebDriver> wait;

        private enum AirBalticControls
        {
            departureInput,
            arrivalInput,

        }

        private Dictionary<AirBalticControls, By> AirBalticElements = new Dictionary<AirBalticControls, By>()
        {
            {AirBalticControls.departureInput, By.CssSelector(".origin input")},
            {AirBalticControls.arrivalInput, By.CssSelector(".destin input")},

        };


        [SetUp]
        public void Initialize()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("--incognito");
            options.AddArguments("--start-maximized");
            options.AddArgument("--disable-popup-blocking");
            driver = new ChromeDriver(options);
            driver.Navigate().GoToUrl(baseUrl);
            wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10)) {Message = "Element was not found"};
        }

        [TearDown]
        public void EndTest() {
            if (driver != null) {
                driver.Quit();
            }
        }

        
        [Test]
        public void KievRigaTest()
        {
            IWebElement departureInput = driver.FindElement(AirBalticElements[AirBalticControls.departureInput]);
            departureInput.Clear();
            departureInput.SendKeys("Kiev");
            UntilElementClickable(By.ClassName("airport")).Click();

            IWebElement arrivalInput = driver.FindElement(AirBalticElements[AirBalticControls.arrivalInput]);
            arrivalInput.Clear();
            arrivalInput.SendKeys("Kiev");
            UntilElementClickable(By.ClassName("airport")).Click();
            /*
            IWebElement kyivAirport = driver.FindElement(AirBalticElements[AirBalticControls.kyivAirport]);
            kyivAirport.Click();

            IWebElement arrivalStation = driver.FindElement(AirBalticElements[AirBalticControls.arrivalStation]);
            arrivalStation.Clear();
            arrivalStation.SendKeys("Copenhagen");

            IWebElement copenhagenAirport = driver.FindElement(AirBalticElements[AirBalticControls.copenhagenAirport]);
            copenhagenAirport.Click();

            IWebElement searchFlightButton = driver.FindElement(AirBalticElements[AirBalticControls.searchFlightButton]);
            searchFlightButton.Click();

            //wait till Show return flights button is loaded
            wait.Until(dr => dr.FindElement(By.XPath("//*[text()=' Show return flights ']")).Displayed);
            IWebElement element = wait.Until(driver => driver.FindElement(By.Name("q")));

            //Assert.AreEqual(true, driver.IsElementDisplayed();
            //Assert.AreEqual(true, driver.IsElementDisplayed(By.XPath("//td[@aria-role = 'button']//p[text()=' Basic ']")));
            //Assert.AreEqual(true, driver.IsElementDisplayed(By.XPath("//td[@aria-role = 'button']//p[text()=' Basic ']")));
            */
        }

        private IWebElement UntilElementClickable(By by)
        {
            wait.Until(dr => dr.FindElement(by).Displayed);
            return driver.FindElement(by);
        }

        public void CheckElementDispalyed(IWebElement element, string control)

        {
            if (element != null)
            {
                Console.WriteLine(control + " is displayed");
            }
            else
            {
                Console.WriteLine(control + " is NOT displayed");
            }

        }
    }

    public static class WebElementExtensions
    {
        public static bool IsElementDisplayed(this IWebDriver driver, By by)
        {
            try
            {
                return driver.FindElement(by).Displayed;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
